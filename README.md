# esa_2020_som

Poster presented at ESA 2020 focusing on the approach and workflow of LTER Soil
Organic Matter synthesis working group. Note that the base construction of the
base construction of the poster was developed in `esa_2020_som.Rmd` but that
the resulting html `esa_2020_som.html` was modified considerably before
printing (to PDF) owing to limited control over formatting with the posterdown
package.
